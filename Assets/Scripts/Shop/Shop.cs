﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{    
    enum ShopItems
    {
        FlameSword,
        BootsOfFlight,
        KeyToCastle
    }

    public GameObject shopPanel;
    public int currentSelectedItem;
    public int currentItemCost;
    private Player player;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            player = other.GetComponent<Player>();
            
            if (player != null)
            {
                UIManager.Instance.OpenShop(player.diamonds);
            }

            shopPanel.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            shopPanel.SetActive(false);
        }
    }

    public void SelectItem(int item)
    {
        //0 = flame sword
        //1 = boots of flight
        //2 = key to castle
        Debug.Log("Item selected " + item);

        switch (item)
        {
            //Числа в параметрах - это координаты по Y Rect Transform компонентов-кнопок FLAME SWORD, BOOTS OF FLIGHT, KEY TO CASTLE, т.е shop меню
            //Сделать попробовать currentSelectedItem через Enum
            //case 0: - так было до

            case (int)ShopItems.FlameSword:
                UIManager.Instance.UpdateShopSelection(69);
                //currentSelectedItem = 0 - было так
                currentSelectedItem = (int)ShopItems.FlameSword;
                currentItemCost = 200;
                break;
            case (int)ShopItems.BootsOfFlight:
                UIManager.Instance.UpdateShopSelection(-31);
                currentSelectedItem = (int)ShopItems.BootsOfFlight;
                currentItemCost = 400;
                break;
            case (int)ShopItems.KeyToCastle:
                UIManager.Instance.UpdateShopSelection(-131);
                currentSelectedItem = (int)ShopItems.KeyToCastle;
                currentItemCost = 100;
                break;
        }
    }

    public void BuyItem()
    {
        if (player.diamonds >= currentItemCost)
        {
            //award item
            if (currentSelectedItem == (int)ShopItems.KeyToCastle)
            {
                GameManager.Instance.HasKeyToCastle = true;
            }

            player.diamonds -= currentItemCost;            
            Debug.Log("Purchased " + currentSelectedItem);
            Debug.Log("Remaining Gems: " + player.diamonds);
            shopPanel.SetActive(false);
        }
        else
        {
            Debug.Log("Not enough of gems");
            shopPanel.SetActive(false);
        }
    }
}
