﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    private Animator animator;
    private Animator animatorSword;
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        animatorSword = transform.GetChild(1).GetComponent<Animator>();
    }
  
    public void Move(float move)
    {
        animator.SetFloat("Move", Mathf.Abs(move));
    }

    public void Jump(bool jumping)
    {
        animator.SetBool("Jumping", jumping);
    }

    public void Attack()
    {
        animator.SetTrigger("Attack");
        animatorSword.SetTrigger("SwordAnimation");
    }

    public void Death()
    {
        animator.SetTrigger("Death");
    }
}
