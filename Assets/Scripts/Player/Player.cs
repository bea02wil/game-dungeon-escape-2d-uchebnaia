﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour, IDamageable
{   
    [SerializeField] float jumpForce = 5.0f;
    [SerializeField] float moveSpeed = 2;
    private Rigidbody2D rigidBody;
    private bool resetJump = false;
    private PlayerAnimation playerAnim;
    private SpriteRenderer playerSprite;
    private SpriteRenderer swordArcSprite;
    public int diamonds;
    private bool grounded = false;
    public int Health { get; set; }

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        playerAnim = GetComponent<PlayerAnimation>();
        playerSprite = GetComponentInChildren<SpriteRenderer>();
        swordArcSprite = transform.GetChild(1).GetComponent<SpriteRenderer>();
        Health = 4;
    }
  
    void Update()
    {
        Movement();

        //Было Input.GetMouseButtonDown(0)
        if (CrossPlatformInputManager.GetButtonDown("A_Button") && IsGrounded())
        {
            playerAnim.Attack();
        }
    }
         
    private void Movement()
    {
        float move = CrossPlatformInputManager.GetAxisRaw("Horizontal"); //Input.GetAxisRaw("Horizontal"); - это для ПК, левая для мобилок и ПК
        grounded = IsGrounded();
        // playerSprite.flipX = move > 0 ? false : true;
        if (move > 0)
        {
            Flip(true);
        }
        else if (move < 0)
        {
            Flip(false);
        }
        //(Input.GetKeyDown(KeyCode.Space)  - было так
        if (CrossPlatformInputManager.GetButtonDown("B_Button") && IsGrounded())
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, jumpForce);
            StartCoroutine(ResetJumpRoutine());
            playerAnim.Jump(true);
        }

        rigidBody.velocity = new Vector2(move * moveSpeed, rigidBody.velocity.y);
        playerAnim.Move(move);
    }
    
    private bool IsGrounded()
    {
        //1 << 8. Битовая операция, сдвиг(шифтинг), напр, 1 << 2 - 0000 0100, 1 << 0 - 0000 0001. Таким образом обращаемся к 8 слою в unity
        //а он там у нас ground. Чтобы луч работал только на Ground(которым помечан Floor в foreground 20-29). Это тоже самое что и LayerMask вариант ниже.
        //RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.down, 1.0f, 1 << 8);
        // RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.down, 0.6f, groundLayer.value);

        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.down, 0.6f, 1 << 8);
        Debug.DrawRay(transform.position, Vector2.down, Color.green);
        if (hitInfo.collider != null) //или if (hitInfo)
        {           
            if (resetJump == false)
            {
                playerAnim.Jump(false);
                return true;
            }
                
        }

        return false;
    }

    private void Flip(bool faceRight)
    {
        if (faceRight == true)
        {
            playerSprite.flipX = false;
            swordArcSprite.flipX = false;
            swordArcSprite.flipY = false;

            Vector2 newPos = swordArcSprite.transform.localPosition;
            newPos.x = 1.01f;
            swordArcSprite.transform.localPosition = newPos;
        }
        else if (faceRight == false)
        {
            playerSprite.flipX = true;
            swordArcSprite.flipX = true;
            swordArcSprite.flipY = true;

            Vector2 newPos = swordArcSprite.transform.localPosition;
            newPos.x = -1.01f;
            swordArcSprite.transform.localPosition = newPos;
        }
    }

    private IEnumerator ResetJumpRoutine()
    {
        resetJump = true;
        yield return new WaitForSeconds(0.1f);
        resetJump = false;
    }

    public void Damage()
    {      
        if (Health < 1)
        {
            return;
        }

        Health--;
        UIManager.Instance.UpdateLives(Health);

        if (Health < 1)
        {
            playerAnim.Death();          
        }
    }

    public void AddGems(int amount)
    {
        diamonds += amount;
        UIManager.Instance.UpdateGemCount(diamonds);
    }
}
