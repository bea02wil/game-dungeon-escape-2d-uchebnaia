﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour, IUnityAdsListener
{
    private string googleGameId = "3788433";
    private string myPlacementId = "rewardedVideo";
    private bool testMode = true;

    private void Start()
    {
        Advertisement.AddListener(this);
        Advertisement.Initialize(googleGameId, testMode);
    }

    public void ShowRewardedAd()
    {
        Debug.Log("Showing Rewarded Ad");

        if (Advertisement.IsReady(myPlacementId))
            Advertisement.Show(myPlacementId);

        else 
            Debug.Log("Rewarded video is not ready at the moment! Please try again later!");


        //Это устарело
        //if (Advertisement.IsReady("rewardedVideo"))
        //{
        //    var options = new ShowOptions() { resultCallback = HandleShowResult};

        //    Advertisement.Show("rewardedVideo", options);            
        //}    
    }

    //Это устарело
    //private void HandleShowResult(ShowResult result)
    //{
    //    switch (result)
    //    {
    //        case ShowResult.Finished:
    //            Debug.Log("Successully finished!");
    //            break;
    //        case ShowResult.Skipped:
    //            Debug.Log("You skipped the ad! No Gems for you!");
    //            break;
    //        case ShowResult.Failed:
    //            Debug.Log("The Video failed, it must not have been ready");
    //            break;
    //    }
    //}

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            GameManager.Instance.Player.AddGems(100);
            UIManager.Instance.OpenShop(GameManager.Instance.Player.diamonds);
        }
        else if (showResult == ShowResult.Skipped)
        {
            Debug.Log("You skipped the ad! No Gems for you!");
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }

    public void OnUnityAdsReady(string placementId)
    {
        
    }

    // When the object that subscribes to ad events is destroyed, remove the listener:
    public void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }
}
