﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton : Enemy, IDamageable
{
    public int Health { get; set; }

    //На C# 8.0 можно было вынести этот общий метод Damage() с Massgiant в реализацию по умолчанию в интерфейс
    public void Damage()
    {
        Debug.Log("Skeleton::Damage()");
        Health--;
        base.animator.SetTrigger("Hit");
        base.isHit = true;
        base.animator.SetBool("InCombat", true);

        if (Health < 1)
        {
            base.animator.SetTrigger("Death");
            base.isDead = true;
            Diamond diamond = Instantiate(base.diamondPrefab, transform.position, Quaternion.identity);
            diamond.gems = base.gems;
        }
    }

    protected override void Movement()
    {
        base.Movement();
    }

    protected override void Init()
    {
        base.Init();
        Health = base.health;
    }
}
