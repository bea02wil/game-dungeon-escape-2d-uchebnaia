﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : Enemy, IDamageable
{
    public GameObject acidEffectPrefab;
    public int Health { get; set; }

    public void Damage()
    {
        Health--;
        if (Health < 1)
        {
            base.animator.SetTrigger("Death");
            base.isDead = true;
            Diamond diamond = Instantiate(base.diamondPrefab, transform.position, Quaternion.identity);
            diamond.gems = base.gems;
        }
    }

    protected override void Update()
    {
        //спецаильно пусто, чтобы убрать Incombat предупреждение, что оно не используется 
    }

    protected override void Init()
    {
        base.Init();
        Health = base.health;
    }

    protected override void Movement()
    {
        //sit still
    }

    public void Attack()
    {
        Instantiate(acidEffectPrefab, transform.position, Quaternion.identity);
    }
}
