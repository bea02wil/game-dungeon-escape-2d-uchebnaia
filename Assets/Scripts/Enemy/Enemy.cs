﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    [SerializeField] protected int health;
    [SerializeField] protected float speed;
    [SerializeField] protected int gems;
    [SerializeField] protected Transform pointA, pointB;
    [SerializeField] protected Diamond diamondPrefab;
    private Vector3 currentTarget;
    protected Animator animator;
    protected SpriteRenderer sprite;
    protected bool isHit = false; 
    protected bool isDead = false;
    private Player player;

    protected virtual void Init()
    {
        animator = GetComponentInChildren<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    protected void Start()
    {
        Init();
    }

    protected virtual void Update()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Idle") && animator.GetBool("InCombat") == false) // || animator.GetCurrentAnimatorStateInfo(0).IsName("Hit")) - тоже, что и isHit переменная
        {
            return;
        }

        if (isDead == false)
            Movement();
    }

    protected virtual void Movement()
    {     
        sprite.flipX = currentTarget == pointA.position ? true : false;

        Vector3 currentPos = transform.position;
        float step = speed * Time.deltaTime; //step - типа скорость. С сайта Unity API

        if (currentPos == pointA.position)
        {
            currentTarget = pointB.position;
            animator.SetTrigger("Idle");
        }
        else if (currentPos == pointB.position)
        {
            currentTarget = pointA.position;
            animator.SetTrigger("Idle");
        }

        if (isHit == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, currentTarget, step);
        }

        float distance = Vector3.Distance(transform.position, player.transform.position);
        if (distance > 4.0f)
        {
            isHit = false;
            animator.SetBool("InCombat", false);
        }

        Vector3 direction = player.transform.position - transform.position;

        if (direction.x > 0 && animator.GetBool("InCombat") == true)
        {
            sprite.flipX = false;
        }
        else if (direction.x < 0 && animator.GetBool("InCombat") == true)
        {
            sprite.flipX = true;
        }
    }
       
}
